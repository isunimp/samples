package com.isunimp.sample.java.netty;

import java.io.*;
import java.net.Socket;

/**
 * @author renguiquan
 * @version $Id: Client.java, v 0.1 2021/7/6 Exp $
 */
public class Client {


    public static void main(String[] args) throws IOException {


        Socket socket = new Socket("192.168.1.4", 8080);

        OutputStream outputStream = socket.getOutputStream();
        InputStream  inputStream  = socket.getInputStream();


        new Thread(() -> {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String line = bufferedReader.readLine();
                    System.out.println(line);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        StringBuffer stringBuffer = new StringBuffer();

        while (true) {

            int read = System.in.read();

            stringBuffer.append((char) read);
            if (read == '\n') {
                outputStream.write(stringBuffer.toString().getBytes());
                stringBuffer = new StringBuffer();
            }

        }
    }

}
