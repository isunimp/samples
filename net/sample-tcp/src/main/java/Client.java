import java.io.*;
import java.net.Socket;

/**
 * @ClassName: Client
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/30
 */
public class Client {

    static public void main(String[] args) {

        try {
            Socket socket = new Socket("127.0.0.1", 3009);
            Writer writer = new OutputStreamWriter(socket.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                writer.write("hello server!\n");
                writer.flush();
                Thread.sleep(2000);
                String msg = reader.readLine();
                System.out.println("收到：" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
