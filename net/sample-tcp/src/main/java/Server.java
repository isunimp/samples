
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName: Server
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/30
 */
public class Server {

    static public void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(3009);
            while (true) {
                final Socket socket = serverSocket.accept();
                System.out.println("收到连接：" + socket.getRemoteSocketAddress().toString());
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            System.out.println(socket);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            Writer writer = new OutputStreamWriter(socket.getOutputStream());
                            while (true) {
                                String msg = reader.readLine();
                                System.out.println("收到消息：" + msg);
                                if (msg == null) {
                                    msg = "000\n";
                                }else {
                                    msg = msg + "\n";
                                }
                                writer.write(msg);
                                writer.flush();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
