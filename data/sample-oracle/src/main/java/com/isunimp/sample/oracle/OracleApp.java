package com.isunimp.sample.oracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * OracleApp class
 *
 * @author isunimp
 * @date 2019/11/14
 */
@SpringBootApplication
public class OracleApp {
    public static void main(String[] args) {
        SpringApplication.run(OracleApp.class, args);
    }
}
