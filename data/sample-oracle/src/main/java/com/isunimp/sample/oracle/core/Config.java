package com.isunimp.sample.oracle.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Config class
 *
 * @author isunimp
 * @date 2019/11/14
 */
@Component
public class Config {
    @Autowired
    DataSource dataSource;

    @PostConstruct
    public void init() throws SQLException {
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(" select * from v$version");
        resultSet.next();
        String s = resultSet.getString(1);
        System.out.println();

    }
}
