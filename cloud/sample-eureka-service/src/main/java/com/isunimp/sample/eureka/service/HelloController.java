package com.isunimp.sample.eureka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HelloController
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/11/20
 */
@RestController
public class HelloController {

    @Autowired
    DiscoveryClient client;

    @GetMapping("/hello")
    public String hello(){
        return "hello world";
    }
}
