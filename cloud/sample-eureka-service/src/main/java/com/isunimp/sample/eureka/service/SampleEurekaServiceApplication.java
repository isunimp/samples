package com.isunimp.sample.eureka.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SampleEurekaServiceApplication {

    class Base<T> {

    }

    class Hello<T> extends Base<T> {

    }

    class test<R> extends Base<Hello<R>> {

    }

    public static void main(String[] args) {
        SpringApplication.run(SampleEurekaServiceApplication.class, args);
    }
}
