import java.util.BitSet;

/**
 * @ClassName: Test
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/6
 */
public class Test {
    /**
     * http://learning.ren/framework/2/1
     *
     * @param args
     * @throws Exception
     */
    static public void main(String[] args) throws Exception {
        CLHLock lock = new CLHLock();
        CLHLock.CLHNode curr = new CLHLock.CLHNode();
        lock.lock(curr);
        new Thread(new Runnable() {
            @Override
            public void run() {
                CLHLock.CLHNode curr1 = new CLHLock.CLHNode();
                lock.lock(curr1);
                System.out.println("222222");
            }
        }).start();

        lock.unlock(curr);
        System.out.println("xxx");
    }

}
