package proxy;

/**
 * @InterfaceName: Hello
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/13
 */
public interface Hello {
    public void say();
}
