package proxy;

import java.lang.reflect.Proxy;

/**
 * @ClassName: proxy.ProxyTest
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/13
 */
public class ProxyTest {

    static public void main(String[] args) {
        HelloProxy proxy = new HelloProxy();
        Hello bookProxy = (Hello) proxy.bind(new HelloImpl());
        bookProxy.say();
    }
}
