package proxy;

/**
 * @ClassName: Hello
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/7/13
 */
public class HelloImpl implements Hello {

    public void say() {
        System.out.println("===============hello");
    }
}
