import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @ClassName: Main
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/12/11
 */
public class Main {

    public static void main(String[] args) {
        int num = 10_000;
        long num1 = 100L;
        System.out.println(num);
        System.out.println(num1);


        AtomicInteger count = new AtomicInteger();
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleWithFixedDelay(() -> {
            System.out.println(count.incrementAndGet());
        }, 0, 1,SECONDS);
    }
}
