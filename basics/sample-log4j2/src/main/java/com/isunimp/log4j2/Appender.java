package com.isunimp.log4j2;

import java.io.Serializable;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Appender class
 *
 * @author isunimp
 * @date 2019/11/21
 */
@Plugin(name = "itc", category = "Core", elementType = "appender", printObject = true)
public class Appender extends AbstractAppender {

    protected Appender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
    }


    @Override
    public void append(LogEvent logEvent) {
        String msg = logEvent.getMessage().getFormattedMessage();
        System.out.println(msg);
    }

    @PluginFactory
    public static Appender createAppender(@PluginAttribute("name") String name,
                                          @PluginElement("Filter") final Filter filter,
                                          @PluginElement("Layout") Layout<? extends Serializable> layout) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new Appender(name, filter, layout);
    }
}
