package com.isunimp.log4j2;

import lombok.extern.slf4j.Slf4j;

/**
 * Main class
 *
 * @author isunimp
 * @date 2019/11/21
 */
@Slf4j
public class Main {

    public static void main(String[] args) {
        log.info("log4j2");
    }

}
