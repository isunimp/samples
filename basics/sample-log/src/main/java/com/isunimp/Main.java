package com.isunimp;

import com.isunimp.a.a1.A;
import com.isunimp.b.B;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Main class
 *
 * @author isunimp
 * @date 2018/8/23
 */
public class Main {

    static Logger logger = LoggerFactory.getLogger(Main.class);
    static public void main(String[] args) {
        new A();
        new B();

        ArrayList ss = new ArrayList();
        ss.add("");
        ss.add("");
        ss.add("");

        try {
            Iterator iterable = ss.iterator();
            while (iterable.hasNext()) {
                ss.remove(iterable.next());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
