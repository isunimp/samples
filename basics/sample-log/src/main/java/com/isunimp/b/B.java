package com.isunimp.b;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * B class
 *
 * @author isunimp
 * @date 2018/8/23
 */
public class B {
    Logger logger = LoggerFactory.getLogger(B.class);

    public B(){
        logger.debug("b.debug");
        logger.info("b.info");
    }
}
