package com.isunimp.a.a1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class
 *
 * @author isunimp
 * @date 2018/8/23
 */
public class A {

    Logger logger = LoggerFactory.getLogger(A.class);

    public A(){
        logger.debug("a.debug");
        logger.info("a.info");
    }
}
