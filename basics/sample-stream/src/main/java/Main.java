import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: Main
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/10/30
 */
public class Main {

    public static void main(String[] args) {
        Map<String, Integer> result = new HashMap<>();
        result.put("1", 1);
        result.put("2", 2);
        result.put("3", 3);

        String s = "   111\n\n";
        String s1 = "111";
        System.out.println(s.trim());
        System.out.println(s1);
    }

}
