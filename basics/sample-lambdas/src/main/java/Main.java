import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @ClassName: Main
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/11/5
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Integer> list = new ArrayList<Integer>(4);
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        list.add(4);
        System.out.println();
        Optional.of(list)
                .filter(list1 -> list1.size() > 0)
                .ifPresent(list1 -> System.out.println(list1));
    }
}
