package com.isunimp.sample.springboot.web.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSpringBootWebKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleSpringBootWebKafkaApplication.class, args);
    }
}
