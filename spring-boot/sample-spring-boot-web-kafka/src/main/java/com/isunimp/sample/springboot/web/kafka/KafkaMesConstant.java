package com.isunimp.sample.springboot.web.kafka;

/**
 * Created by baipan
 * Date: 2018-03-07
 */
public class KafkaMesConstant {

    static final String SUCCESS_CODE = "00000";
    static final String SUCCESS_MES = "成功";

    /*kakfa-code*/
    static final String KAFKA_SEND_ERROR_CODE = "30001";
    static final String KAFKA_NO_RESULT_CODE = "30002";
    static final String KAFKA_NO_OFFSET_CODE = "30003";

    /*kakfa-mes*/
    static final String KAFKA_SEND_ERROR_MES = "发送消息超时,联系相关技术人员";
    static final String KAFKA_NO_RESULT_MES = "未查询到返回结果,联系相关技术人员";
    static final String KAFKA_NO_OFFSET_MES = "未查到返回数据的offset,联系相关技术人员";
}
