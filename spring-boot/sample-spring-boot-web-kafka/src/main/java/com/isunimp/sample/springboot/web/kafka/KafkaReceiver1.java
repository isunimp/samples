package com.isunimp.sample.springboot.web.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @ClassName: KafkaReceiver1
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/10/7
 */
@Component
@Slf4j
public class KafkaReceiver1 {
    @KafkaListener(topics = {"test"})
    public void listen(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
            log.info("=====KafkaReceiver1:" + record);
            log.info("=====KafkaReceiver1:" + message);
        }
    }
}
