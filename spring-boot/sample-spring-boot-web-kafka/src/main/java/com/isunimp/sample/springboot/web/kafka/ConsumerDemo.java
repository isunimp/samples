package com.isunimp.sample.springboot.web.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * Date: 2018-03-27
 */
public class ConsumerDemo {


    public static void main(String[] args) {

        String str = "D76AB2A6D1353A8800010000005542039ED7356069252427132EBD90303687AB824E4BA4F5F9EF40EEE978663A106C18DE9807255056DB730D870256BC0F2EBAD82EFFE951755F1CE1E6B93C58771891369E9022E607F27DB5732B6EBDA7A49F3FEB11";
        byte[] bytes = Base64.getDecoder().decode(str);
        System.out.println(new String(bytes));

        Properties props = new Properties();
        props.put("bootstrap.servers", "tkaf01:9092");
//        props.put("bootstrap.servers", "QT02:9092,KAF01:9092,KAF02:9092");
//        props.put("bootstrap.servers", "47.93.224.229:9092");
//        props.put("bootstrap.servers", "QT02:2181,KAF01:2181,KAF02:2181");
        props.put("group.id", "ipp_test_local3_"+System.currentTimeMillis());
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList("message_publish", "message_publish_app", "client_connected", "client_disconnected"));

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {

            }
        }
    }

}
