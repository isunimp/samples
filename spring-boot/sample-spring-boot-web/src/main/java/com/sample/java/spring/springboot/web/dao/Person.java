package com.sample.java.spring.springboot.web.dao;


import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/14
 **/
public interface Person {

    @Select("select * from person")
    List<com.sample.java.spring.springboot.web.entity.Person> select();

    @Insert("insert into person value(1, '1', 1)")
    int inert();

}
