package com.sample.java.spring.springboot.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/18
 **/
@Component
public class Config {

    static public String NAME;


    public void setNAME(String NAME) {
        Config.NAME = NAME;
    }
}
