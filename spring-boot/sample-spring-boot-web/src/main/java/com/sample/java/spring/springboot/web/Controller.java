package com.sample.java.spring.springboot.web;

import com.sample.java.spring.springboot.web.config.Config;
import com.sample.java.spring.springboot.web.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: Test
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/8/1
 */
@RestController
@Validated
public class Controller {
    @Value("${PID}")
    Integer pid;

    @Value("${name}")
    String name;

    @Autowired
    PersonService service;


    @GetMapping("/**")
    public Object forwarding(HttpServletRequest request) {
        System.out.println(request.getMethod() + ":" + request.getRequestURL());
        return "ok";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String fun1() {
        System.out.println(Config.NAME);
//        service.inert();
        return "hello:" + Config.NAME;
    }

    @PostConstruct
    public void xxx() {
        System.out.println(Config.NAME);
    }

}
