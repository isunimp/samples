package com.sample.java.spring.springboot.web.service;

import com.sample.java.spring.springboot.web.dao.Person;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/14
 **/
@Service
public class PersonService {

    @Resource
    Person person;

    public void inert() {
        person.inert();
        throw new RuntimeException();
    }

}
