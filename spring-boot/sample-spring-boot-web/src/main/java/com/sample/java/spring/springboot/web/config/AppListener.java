package com.sample.java.spring.springboot.web.config;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/23
 **/
@Component
public class AppListener implements ApplicationListener {

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        System.out.println("zzz " + applicationEvent.getClass().getName());
    }
}
