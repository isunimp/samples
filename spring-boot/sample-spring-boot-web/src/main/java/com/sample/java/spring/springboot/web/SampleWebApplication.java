package com.sample.java.spring.springboot.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@MapperScan("com.sample.java.spring.springboot.web.dao")
public class SampleWebApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SampleWebApplication.class);
        application.addListeners(new ApplicationListener<ApplicationEnvironmentPreparedEvent>() {
            @Override
            public void onApplicationEvent(ApplicationEnvironmentPreparedEvent applicationEvent) {
                Map<String, Object>                 property                 = new HashMap<>();
                property.put("name", "renguiquan");
                MapPropertySource propertySource = new MapPropertySource("renguiquanadd", property);
                applicationEvent.getEnvironment().getPropertySources().addLast(propertySource);
                System.out.println(applicationEvent.toString());
            }
        });
        application.run(args);
    }
}
