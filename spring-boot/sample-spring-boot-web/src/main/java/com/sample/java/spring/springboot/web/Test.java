package com.sample.java.spring.springboot.web;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

/**
 * @ClassName: Test
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/8/1
 */
@RestController
public class Test implements ApplicationContextAware {
    @Value("${PID}")
    Integer pid;

    @Value("${name}")
    String name;

    @Autowired
    A a;

    ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String fun1(){
        return "hello";
    }

    public Test(){
        System.out.println();
    }


    @PostConstruct
    public void loadUsers() {

    }
}
