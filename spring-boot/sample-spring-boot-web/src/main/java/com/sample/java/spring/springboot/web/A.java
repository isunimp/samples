package com.sample.java.spring.springboot.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A class
 *
 * @author isunimp
 * @date 2019/9/29
 */

@Component
public class A {

    @Autowired
    Test test;

}
