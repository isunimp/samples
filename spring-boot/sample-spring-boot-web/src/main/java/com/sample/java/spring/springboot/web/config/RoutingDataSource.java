package com.sample.java.spring.springboot.web.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/14
 **/
//@Component
public class RoutingDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return null;
    }
}
