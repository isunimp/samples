package controller;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * @ClassName: Controller
 * @Description: TODO
 * @Auther: isunimp
 * @Date: 2018/5/21
 */

@org.springframework.stereotype.Controller
@RequestMapping("/test")
public class Controller {


    @RequestMapping("hello")
    public String test(Model model){
        model.addAttribute("name", "isunimp");
        model.addAttribute("list", Arrays.asList("q","w","e"));
        ArrayList<List<String>> citys = new ArrayList<>();
        citys.add(Arrays.asList("北京","北京","0001"));
        citys.add(Arrays.asList("广东","广州","0002"));
        citys.add(Arrays.asList("四川","成都","0003"));
        model.addAttribute("citys", citys);
        return "/test/hello";
    }
}
