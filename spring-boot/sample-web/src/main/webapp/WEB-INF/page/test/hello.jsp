<%--
  Created by IntelliJ IDEA.
  User: isunimp
  Date: 2018/5/21
  Time: 下午5:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>hello</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<h1>test</h1>
<p>${name }</p>
<p><c:out value="${name}"></c:out></p>
<c:if test="${1 < 2}">
    <p>true</p>
</c:if>


<c:forEach var="idx" items="${list}">
    <%--<span>${idx}</span>--%>
    <p>${idx}</p>
</c:forEach>


<table class="table table-striped">
    <caption>条纹表格布局</caption>
    <thead>
    <tr>
        <th>名称</th>
        <th>城市</th>
        <th>邮编</th>
    </tr>
    </thead>
    <tbody>

    <c:out value="${citys}"></c:out>

    <c:forEach var="city" items="${citys}">
        <tr>
            <c:forEach items="${city}" var="info">
                <td>${info}</td>
            </c:forEach>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
