package com.isunimp.sample.springboot.web.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class SampleSpringBootWebAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleSpringBootWebAopApplication.class, args);
    }
}
