package com.isunimp.sample.springboot.web.aop.controller;

import com.isunimp.sample.springboot.web.aop.service.Service;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: TestController
 * @Description: TODO
 * @Author: isunimp
 * @Date: 2018/8/7
 */
@RestController
public class TestController {

    @Autowired
    HttpServletRequest request;

    @Resource
    Service service;

    @PostMapping("/test/{id}")
    public String test(@PathVariable String id, @RequestBody String s) {
        System.out.println("test:" + id);
        try {
            ((TestController) AopContext.currentProxy()).test1();
        } catch (Exception e) {

        }
        System.out.println(request);
        return "";
    }

    @PostConstruct
    public void xxx(){
        service.xxx();
    }

    public void test1(){

    }
}
