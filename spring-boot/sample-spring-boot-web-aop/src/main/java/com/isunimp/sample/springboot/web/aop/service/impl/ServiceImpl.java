package com.isunimp.sample.springboot.web.aop.service.impl;

import com.isunimp.sample.springboot.web.aop.service.Service;

/**
 * @description:
 * @author: renguiquan
 * @create: 2019/10/14
 **/
@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

    @Override
    public void xxx() {
        System.out.println("xxx");
    }
}
